data "aws_caller_identity" "current" {}

data "aws_caller_identity" "network" {
  provider = aws.network
}

data "aws_region" "current" {}
