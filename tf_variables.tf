// Required for provider
variable "region" {
  type        = string
  description = "The region of the account"
}

// Organization specific tags


variable "account_name" {
  type        = string
  description = "Name of the current account."
}

variable "system" {
  type        = string
  description = "Name of a dedicated system or application"
}

variable "stage" {
  type        = string
  description = "Name of a dedicated system or application"
}

// Tags
variable "tags" {
  type        = map(string)
  description = "Tag that should be applied to all resources."
  default     = {}
}

// Module specific variables
variable "ram_principal" {
  type        = string
  description = "The principal account for the resource share"
}

variable "ram_share_arns" {
  type        = list(string)
  description = "List of resource share ARNs"
}
