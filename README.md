# Account Core Module

The account core module is to be rolled out in every AWS Account.

It basically create IAM roles that can be accessed from one or more
principals that are declared in an input variable. Examples are other
AWS accounts (user mananagement account) or a federated identity
provider such as Microsoft Active Directory.

Roles are defined by role policy attachments (RPAs). The module will
create a new role for every distinct role name in the RPA variable.

Policies that shall be attached to these roles are identified by ARNs
and need to be created outside the module. A future extension of this
module might include dynamic policy creation.

By default, it will create a AdminRole and a ReadOnlyRole. These can
be disabled by setting standard_role_policy_attachments = false.

Furthermore, service roles for AWS Config and others are created.

## Contributing
This module is intended to be a shared module.
Please don't commit any customer-specific configuration into this module and keep in mind that changes could affect other projects.

For new features please create a branch and submit a pull request to the maintainers.

Accepted new features will be published within a new release/tag.

## Pre-Commit Hooks

### Enabled hooks
- id: end-of-file-fixer
- id: trailing-whitespace
- id: terraform_fmt
- id: terraform_docs
- id: terraform_validate
- id: terraform_tflint

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.2.0 |
| <a name="provider_aws.network"></a> [aws.network](#provider\_aws.network) | 4.2.0 |
| <a name="provider_null"></a> [null](#provider\_null) | 3.1.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_ram_principal_association.network_shares](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ram_principal_association) | resource |
| [null_resource.tag_network_resources](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_caller_identity.network](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_account_name"></a> [account\_name](#input\_account\_name) | Name of the current account. | `string` | n/a | yes |
| <a name="input_ram_principal"></a> [ram\_principal](#input\_ram\_principal) | The principal account for the resource share | `string` | n/a | yes |
| <a name="input_ram_share_arns"></a> [ram\_share\_arns](#input\_ram\_share\_arns) | List of resource share ARNs | `list(string)` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | The region of the account | `string` | n/a | yes |
| <a name="input_stage"></a> [stage](#input\_stage) | Name of a dedicated system or application | `string` | n/a | yes |
| <a name="input_system"></a> [system](#input\_system) | Name of a dedicated system or application | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Tag that should be applied to all resources. | `map(string)` | `{}` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
