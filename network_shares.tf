resource "aws_ram_principal_association" "network_shares" {
  for_each = toset(var.ram_share_arns)
  provider = aws.network

  principal          = var.ram_principal
  resource_share_arn = each.value
}
