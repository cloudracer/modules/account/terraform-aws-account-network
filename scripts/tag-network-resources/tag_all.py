#!/usr/bin/python3

#################################
# - Imports & Global Variables- #
#################################

import argparse # Argument parser
import sys # OS System Service
import boto3 # AWS SDK
import json # To work with JSON data
import os # To get the current working directory

clients = {} # Holds all AWS Clients

total_source_resources_found = 0
total_target_resources_found = 0
get_data_errors = 0
matching_resources = 0
resources_tagged = 0
resources_with_no_tags = 0

##################################
# - Helper Functions & Classes - #
##################################

# Simple Class that creates an own exception
class QuitCodeException(Exception):
    pass

# Receives Python Script Arguments
def parseArguments():
  parser = argparse.ArgumentParser() # Initiate the parser
  parser.add_argument("--ownerrole", "-o", help="IAM Role with access to the resource owner account.")
  parser.add_argument("--targetrole", "-t", help="IAM Role with access to receiving AWS account.")
  parser.add_argument("--region", "-r", help="AWS Region")
  args = parser.parse_args()
  if(args.ownerrole and args.targetrole and args.region):
    print("- Arguments received: ", args.ownerrole, args.targetrole, args.region)
  else:
    raise QuitCodeException("--ownerrole, --targetrole and --region needs to be specified.")

  return args # Return the Arguments from the CLI

# Create AWS Clients to work with the AWS SDK
def createAwsClients(ownerrole, targetrole, region):
  global clients

  print('- Creating AWS Service Clients...')

  # Setup STS Clients
  sts_owner = boto3.client('sts', region_name='eu-central-1')
  sts_target = boto3.client('sts', region_name='eu-central-1')

  # Assume Roles in Owner & Target Accounts
  try:
    assumed_role_object_owner = sts_owner.assume_role(RoleArn=ownerrole, RoleSessionName='PythonTagNACLs') # Assume role in owner account
  except Exception as e:
    raise QuitCodeException('Could not Assume Owner Role. Details: {0}'.format(e))
  try:
    assumed_role_object_target = sts_target.assume_role(RoleArn=targetrole, RoleSessionName='PythonTagNACLs') # Assume role in target account
  except Exception as e:
    raise QuitCodeException('Could not assume Target Role. Details: {0}'.format(e))

  # Read Role Credentials
  credentials_owner = assumed_role_object_owner['Credentials']
  credentials_target = assumed_role_object_target['Credentials']

  # Create EC2 Owner Client
  try:
    clients['ec2_owner'] = boto3.client('ec2',
      region_name='eu-central-1',
      aws_access_key_id=credentials_owner['AccessKeyId'],
      aws_secret_access_key=credentials_owner['SecretAccessKey'],
      aws_session_token=credentials_owner['SessionToken'],
    )
  except Exception as e:
    raise QuitCodeException('Could not create EC2 Owner Client. Details: {0}'.format(e))

  # Create EC2 Target Client
  try:
    clients['ec2_target'] = boto3.client('ec2',
      region_name='eu-central-1',
      aws_access_key_id=credentials_target['AccessKeyId'],
      aws_secret_access_key=credentials_target['SecretAccessKey'],
      aws_session_token=credentials_target['SessionToken'],
    )
  except Exception as e:
    raise QuitCodeException('Could not create EC2 Target Client. Details: {0}'.format(e))

# Iterates over the Resources
def iterateOverResources(resource_target_data, resourceKey, resourceIdKey, resource_owner_data):
  print('- Started Tagging process for: {0}'.format(resourceKey))

  for resource in resource_target_data:
    resource_id = getResourceID(resource, resourceIdKey)
    tags = findMatchingTags(resource_id, resourceIdKey, resource_owner_data) # Find the Matching Tags
    if(tags != False):
      tagAWSResource(resource_id, tags, clients['ec2_target']) # Tags a Resource

# Gets the Resource ID
def getResourceID(resource, id_key):
  print('-- Trying to find the ID for the AWS Resource')

  if(id_key in resource):
    resource_id = resource[id_key]
    print('--- Found Resource ID: {0}'.format(resource_id))
    return resource_id
  else:
    print('--- Error! Resource ID not found in Resource Object! Cannot proceed')
    return None

# Finds matching Tags
def findMatchingTags(resource_id, resource_key, resources_owner_data):
  global matching_resources, resources_with_no_tags

  print("-- Searching for Tags for Resource ID:", resource_id)
  tags = None
  for owner_resource in resources_owner_data:
    if(owner_resource[resource_key] == resource_id):
      matching_resources += 1
      if('Tags' in owner_resource):
        tags = owner_resource['Tags']
        print("--- Found Resource Tags: ", tags)
        return tags
      else:
        print('--- Error! Owner Resource ID {0} has no Tags!'.format(resource_id))
        return None

# Tags an AWS Resource
def tagAWSResource(resource_id, tags, client):
  global resources_tagged, resources_with_no_tags

  print("-- Adding Tags for AWS Resource ID: ", resource_id)
  if((tags != None) and (tags != "")):
    try:
      client.create_tags(Resources=[resource_id], Tags=tags)
    except Exception as e:
      print('--- Error! Could not Tag AWS Resource: {0} with provided Owner Account Tags {1}! Details: {2}'.format(resource_id, tags, e))
    else:
      resources_tagged += 1
  else:
    print('--- Error! Resource ID {0} has no Tags!'.format(resource_id))
    resources_with_no_tags += 1

# Fetches Resource Data from any given resource (recursive function)
def getResourceData(client, data_key, data_function, next_token = None, round_num = 1):
  global get_data_errors, total_source_resources_found, total_target_resources_found

  resource_data = [] # List that holds all resources
  resources = {} # Object for the response
  if(next_token == None):
    print('- Fetching all Resources from type: {0} from Account: {1} ...'.format(data_key, client))
    function_name = "clients['"+client+"']."+data_function+"()" # We are constructing our function call here
    try:
      resources = eval(function_name) # Call the describe function the first time
    except Exception as e:
      print('-- Error! Could not fetch Resources from type: {0} from Account: {1}! Details: {2}'.format(data_key, client, e))
      get_data_errors  += 1
  else: # Next Token, another round
    print('- Fetching all Resources from type: {0} from Account: {1}. Round: {2} ...'.format(data_key, client, round_num))
    function_name = "clients['"+client+"']."+data_function+"(, NextToken='"+next_token+"')" # We are constructing our function call here
    try:
      resources = eval(function_name) # Call the describe function another time with the "NextToken"
    except Exception as e:
      print('-- Error! Could not fetch Resources from type: {0} from Account: {1}! Details: {2}'.format(data_key, client, e))
      get_data_errors  += 1

  if(data_key in resources):
    resource_data.extend(resources[data_key])
  else:
    print('-- Error! No Resources from type: {0} found in Resource object for Account: {1}!'.format(data_key, client))

  if('NextToken' in resources):
    round_num += 1
    resource_data.extend(getResourceData(client, data_key, data_function, next_token = resources['NextToken'], round_num = round_num))

  if(client == 'ec2_owner'):
    total_source_resources_found += len(resource_data)
  else:
    total_target_resources_found += len(resource_data)

  return resource_data

# Loads the JSON config file
def loadJSONFile(file):
  currentDirectory = os.path.dirname(os.path.abspath(__file__))
  total_file_path = currentDirectory+'/'+file
  try:
    f = open (total_file_path, "r")
  except Exception as e:
    print('- Error opening JSON File with all Resource Configs. Cannot proceed! Details: ', e)
    exit()

  try:
    data = json.loads(f.read())
  except Exception as e:
    print('- Error converting contents of JSON File with all Resource Configs to Python object. Cannot proceed! Details: ', e)
    exit()

  f.close()

  if('resources' in data):
    return data['resources']
  else:
    print('- Error parsing JSON File with all Resource Configs. Cannot proceed!')
    exit()

# Tags all Resources in the Target Account
def tagResources():
  resources = loadJSONFile('resources.json')
  print('- Reading all Resource information from the Source and Target Accounts...')
  for resource in resources:
    # Receive all necessary data
    resource_source_data = getResourceData('ec2_owner', resource['data_key'], resource['function_name'])
    resource_target_data = getResourceData('ec2_target', resource['data_key'], resource['function_name'])
    # Loop over all AWS Resources
    iterateOverResources(resource_target_data, resource['data_key'], resource['id_key'], resource_source_data)

############
# - MAIN - #
############
def main():
  print('Tagging Script started...\n')

  try:
    args = parseArguments() # Parse Arguments
  except QuitCodeException as e:
    print("- Error: ", e)
    exit()

  try:
    createAwsClients(args.ownerrole, args.targetrole, args.region)
  except QuitCodeException as e:
    print("- Error: ", e)
    exit()

  tagResources() # Tag all the AWS Resources

  print('\nAll Resources tagged! Results:')
  print('- Target Account: ', args.targetrole.split(':')[4])
  print('- Total Source Resources: ', total_source_resources_found)
  print('- Total Target Resources: ', total_target_resources_found)
  print('- Errors retrieving Resources: ', get_data_errors)
  print('- Matching Source & Target Resources: ', matching_resources)
  print('- Resources Tagged: ', resources_tagged)
  print('- Resources with no Tags: ', resources_with_no_tags)

if __name__ == "__main__":
    main()
