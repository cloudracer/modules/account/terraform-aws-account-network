# Tag all Resources

## Overview

This Python Script reads all Tags from the Resources of the Network Account and copies them to the shared Resources in the other Accounts.

## Usage

- `python3 modules/account_core/scripts/tag_all.py -o arn:aws:sts::170590071797:role/OrganizationAccountAccessRole -t arn:aws:sts::862729034627:role/OrganizationAccountAccessRole -r eu-central-1`
