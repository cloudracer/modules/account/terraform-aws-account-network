locals {
  tags_module = {
    Terraform               = true
    Terraform_Module        = "terraform-aws-account-network"
    Terraform_Module_Source = "https://gitlab.com/tecracer-intern/terraform-landingzone/modules/terraform-aws-account-network"
    Stage                   = var.stage
    System                  = var.system
  }
  tags = merge(local.tags_module, var.tags)
}

locals {
  current_role_arn = "${replace(split("/", data.aws_caller_identity.current.arn)[0], "assumed-role", "role")}/${split("/", data.aws_caller_identity.current.arn)[1]}"
  network_role_arn = "${replace(split("/", data.aws_caller_identity.network.arn)[0], "assumed-role", "role")}/${split("/", data.aws_caller_identity.network.arn)[1]}"
}
